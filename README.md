Me gustó este sistema de guías, a pesar de no haberlo aprovechado desde el principio, siento que aprendo más y mejor 
teniendo la posibilidad de ver las clases todas las veces que necesite y retrocediendo para volver a donde perdí el 
hilo.

Quiero excusarme, profesor, por no haberle dedicado más tiempo a las primeras guías, realmente me atacó un cambio de
prioridades inesperado en las primeras semanas de esta unidad.

Notará que dediqué menos tiempo a las guías 1 y 2 que a la guía 3. Pero también siento que me merezco una palmada en 
la espalda de mi parte por lo que logré hacer en 1 semana de trabajo con 2 pruebas en paralelo a las guías.

Le confieso también un pecado que cometí con la guía 1, que, puesto que en un momento me vi en desesperación por mi 
poco tiempo le copié la exigencia n° 10 de su sugerencia de guía. El resto de mis copias de funciones y métodos fueron
hechas directamente mirando sus clases, por lo que no siento que sea tan grave, ya que significó entender y aprender haciendo. De haberle dedicado más tiempo podría haber logrado un mejor manejo de los datos y probablemente habrían 
resultados con más sentido entre mis gráficos (creo que notará las inconsistencias debido a datos "NA" entre mis 
gráficos).

Creo que eso es todo lo que puedo decir, espero que mi autocrítica le ablande un poco el corazón para que no me la 
deje tan difícil con la recuperativa y el examen. :c